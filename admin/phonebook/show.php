<?php
//connect to database
$servername = "localhost";
$username = "root";
$password = "1234";
$dbname = "phpcrud";

//connecting to database
$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//selection query
$query = 'SELECT * FROM phonebooks WHERE id =:id ORDER BY id DESC';
$sth = $conn->prepare($query);
$sth ->bindParam(':id',$_GET['id']);
$sth->execute();

$phonenumber = $sth->fetch(PDO::FETCH_ASSOC);

/*echo "<pre>";
print_r($phonenumber);
echo "</pre>";*/
?>

<!doctype html> 
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Show Phone Number</title>
</head>
<body>
<dl>
    <dt>ID</dt>
    <dd><?=$phonenumber['id']?></dd>
    <dt>Phone Number</dt>
    <dd><?=$phonenumber['number']?></dd>
</dl>
</body>
</html>
