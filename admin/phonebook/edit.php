<?php
//connect to database
$servername = "localhost";
$username = "root";
$password = "1234";
$dbname = "phpcrud";

//connecting to database
$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//selection query
$query = 'SELECT * FROM phonebooks WHERE id =:id';
$sth = $conn->prepare($query);
$sth ->bindParam(':id',$_GET['id']);
$sth->execute();

$phonenumber = $sth->fetch(PDO::FETCH_ASSOC);

/*echo "<pre>";
print_r($phonenumber);
echo "</pre>";*/
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Phonebook edit</title>
</head>
<body>
<form action = "update.php" method="post">
    <fieldset>
        <legend>Edit the Phone Number</legend>
        <div>
            <label for = "phonenumber">Enter Phone Number</label>
            <input
                type="text"
                name = "id"
                autofocus="autofocus"
                placeholder="e.g +8801675 08 56 31" 
                value = <?php echo $phonenumber['id'];?>

            />
            <input
                type="text"
                name = "number"
                autofocus="autofocus"
                placeholder="e.g +8801675 08 56 31"
                value="<?php echo $phonenumber['number']?>"

            />
        </div>
        <button type="submit">Save</button>
    </fieldset>
</form>
</body>
</html>