<?php
//collect the data
$id = $_POST['id'];
$title = $_POST['title'];

include_once ($_SERVER["DOCUMENT_ROOT"]."/phpcrud-master/connection.php");

$query = "UPDATE `products` SET `title` = :title WHERE `products`.`id` = :id";

$sth = $conn->prepare($query);
$sth->bindParam(':id',$id);
$sth->bindParam(':title',$title);
$result = $sth->execute();

//redirect to index page
header("location:index.php");
