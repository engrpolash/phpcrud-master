<?php
//collect the data
$id = $_GET['id'];
//connect to database
include_once ($_SERVER["DOCUMENT_ROOT"]."/phpcrud-master/connection.php");

$query = "DELETE FROM `products` WHERE `products`.`id` = :id";

$sth = $conn->prepare($query);
$sth->bindParam(':id',$id);
$result = $sth->execute();

//redirect to index page
header("location:index.php");