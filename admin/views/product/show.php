<?php
//connect to database
include_once ($_SERVER["DOCUMENT_ROOT"]."/phpcrud-master/connection.php");

//selection query
$id = $_GET ['id'];
$query = 'SELECT * FROM products WHERE id = :id';
$sth = $conn->prepare($query);
$sth -> bindParam(':id',$id);
$sth->execute();

$product = $sth->fetchAll(PDO::FETCH_ASSOC);
?>

<?php
ob_start();
include_once ($_SERVER["DOCUMENT_ROOT"]."/phpcrud-master/admin/views/layouts/admin.php");
$layout = ob_get_contents();
ob_end_clean();
?>

<?php
ob_start();
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
        <h1 >Product</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <button type="button" class="btn btn-sm btn-outline-secondary">
                <span data-feather="calendar"></span>
                <a href="<?=VIEW;?>product/index.php" style="color: black">Go to List</a>
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ">
            <section >
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm col-md-6 col-lg-3 ">
                            <div class="product">
                                <a href="#" class="img-prod"><img class="img-fluid" src="<?php echo $product['picture']?>" alt="Colorlib Template">
                                    <span class="status">30%</span>
                                </a>
                                
                                <div class="text">
                                    <h3><a href ="#"><?=$product['title']?></a></h3>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>



</main>

<?php
$pageContent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##",$pageContent,$layout);
?>



